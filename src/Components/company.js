import React, { Component } from 'react';
import ModalsAdd from './user';
import Table from './table';
class Forms extends Component {
    constructor()
    {
        super();
        this.state={
            modalShow:false, 
            fields:{companyName:'',companyEmail:'',companyPhone:'',companyWurl:'',companyAddress:''},
            userRecord:[{firstName:'',lastName:'',email:'',vaccinated0:'No'}],
            data:[]
        };
        this.handleState=this.handleState.bind(this);
        this.modalClose=this.modalClose.bind(this)
        this.addUser=this.addUser.bind(this);
        this.deleteUser=this.deleteUser.bind(this);
        this.handleInputChange=this.handleInputChange.bind(this);
        this.confrimCheck=this.confrimCheck.bind(this);
    }
    handleInputChange(e,index){
        let value=this.state.userRecord;
        value[index][e.target.name]=e.target.value;
        this.setState({
            userRecord:value
        });
    }
    addUser(index){
        this.setState({
            userRecord:[...this.state.userRecord,{firstName:'',lastName:'',email:'',["vaccinated"+index]:'No'}]});
    }

    deleteUser(e,index){
        
        let value=this.state.userRecord;
        value.splice(index,1);
        this.setState({
            userRecord:value});
    }
    handleState()
    {
            this.setState({modalShow:true});
    }
    modalClose(){
            this.setState({
            modalShow:false,
            userRecord:[{firstName:'',lastName:'',email:'',vaccinated0:'No'}],
            fields:{companyName:'',companyEmail:'',companyPhone:'',companyWurl:'',companyAddress:''}
        });
    }
    handleCheck(e){
        let value=this.state.fields;
        value[e.target.name]=e.target.value;
        this.setState({fields:value});
    }
    confrimCheck(){
        const obj={company:this.state.fields,user:this.state.userRecord}
        const arr=[...this.state.data];
        arr.push(obj);
        this.setState({
            data:arr,
            userRecord:[{firstName:'',lastName:'',email:'',vaccinated0:'No'}],
            fields:{companyName:'',companyEmail:'',companyPhone:'',companyWurl:'',companyAddress:''}
        });
        this.modalClose();
    }
    render() { 
         let temp=false;
         if(Object.values(this.state.fields).some(a=>!a))
         {
             temp=true;
         }
         else{
             if(/\d/.test(this.state.fields.companyName)){
                 temp=true;
             }
             if((/[a-zA-Z]/g).test(this.state.fields.companyPhone))
             {
                 temp=true;
             }
             if(!/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,4}$/.test(this.state.fields.companyEmail))
             {
                temp=true;
             }
             if(!/\./g.test(this.state.fields.companyWurl))
             {
                temp=true;
             }
         }
        return (
                <React.Fragment>
                    <div className="row">
                        <div className="col-md-3 offset-md-2">
                            <div className="try">
                                <label className="col-sm-12 mt-0 pl-0" htmlFor="companyName">Name:</label>
                                <input className="col-sm-10 mt-0" 
                                type="text" 
                                name="companyName" 
                                id="companyName" 
                                value={this.state.fields.companyName}
                                onChange={e=>this.handleCheck(e)}/>
                                <span>Name contains only Alphabets</span>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="try">
                                <label className="col-sm-12 mt-0 pl-0" htmlFor="companyEmail">Email:</label>
                                <input className="col-sm-10 mt-0" type="email" id="companyEmail" name="companyEmail" 
                                value={this.state.fields.companyEmail}
                                onChange={e=>this.handleCheck(e)}/>
                                <span>ValidEmail ex:abcd@abcd.abc</span>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="try">
                                <label className="col-sm-12 mt-0 pl-0" htmlFor="companyPhone">Phone:</label>
                                <input className="col-sm-10 mt-0" 
                                type="text" 
                                id="companyPhone" 
                                name="companyPhone" 
                                value={this.state.fields.companyPhone}
                                onChange={e=>this.handleCheck(e)}/>
                                <span>Field must be a numbers</span>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 offset-md-2">
                            <div className="try">
                                <label className="col-sm-12 mt-0 pl-0" htmlFor="companyWurl">Website URL:</label>
                                <input className="col-sm-10 mt-0" 
                                type="text" 
                                id="companyWurl" 
                                name="companyWurl" 
                                value={this.state.fields.companyWurl}
                                onChange={e=>this.handleCheck(e)}/>
                                <span>Give valid url</span>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="try">
                                <label className="col-sm-12 mt-0 pl-0" htmlFor="companyAddress">Address:</label>
                                <textarea className="col-sm-10 mt-0"  
                                id="companyAddress" 
                                name="companyAddress" 
                                value={this.state.fields.companyAddress}
                                onChange={e=>this.handleCheck(e)}></textarea>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-2 offset-md-2">
                            <div className="ps-4 try">
                                <button 
                                disabled={temp}
                                className="btn py-1 px-3 btn-primary" 
                                onClick={this.handleState}>Add User</button>

                                <ModalsAdd 
                                show={this.state.modalShow}
                                addUser={this.addUser}
                                deleteUser={this.deleteUser}
                                handleInputChange={this.handleInputChange} 
                                user={this.state.userRecord}
                                confrimCheck={this.confrimCheck} 
                                modalClose={this.modalClose}/>
                            </div>
                        </div>
                    </div>
                    <Table data={this.state.data}/>
            </React.Fragment>
        );
    }
}
 
export default Forms;