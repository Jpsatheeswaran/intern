import React, { Component } from 'react';
class Table extends Component {
    
    render() {
        return (  
            
            (this.props.data.length>0) ? 
            <div className="row mt-5">
                <div className="col-md-10 offset-md-1">
                    <div className="try">
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th>companyName</th>
                                    <th>firstName</th>
                                    <th>lastName</th>
                                    <th>Email</th>
                                    <th>Vaccinated</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.props.data.map((a,outerIndex)=>{
                                       return( a.user.map((r,innerIndex)=>{
                                            return(
                                                <tr key={outerIndex+innerIndex}>
                                                    <td>{a.company.companyName}</td>
                                                    <td>{r.firstName}</td>
                                                    <td>{r.lastName}</td>
                                                    <td>{r.email}</td>
                                                    <td>{r["vaccinated"+innerIndex]}</td>
                                                </tr>)
                                        })
                                        )
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            :<div></div>
        );
    }
}
 
export default Table;