import React, { Component } from 'react';
import {Button,Modal,Container,Row,Col} from "react-bootstrap";
import { Radio, RadioGroup} from 'react-radio-group';
class  ModalsAdd extends Component {
    constructor(){
        super();
        this.state={bool:'No'};
        this.radioButton=this.radioButton.bind(this);
    }
    radioButton(e,i)
    {
        this.props.handleInputChange(e,i);
    }
    render() {
         let empt=false;
         this.props.user.map((item)=>{
            if(Object.values(item).some((val)=>!val))
            {
                empt=true;

            }
            else{
                if(!/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,4}$/.test(item.email))
                {
                    empt=true;
                }

                if((/\d/.test(item.lastName))||(/\d/.test(item.fisrtName)))
                {
                    empt=true;
                }
                
            }
            return" ";
        });
        return (  
            <Modal
                show={this.props.show}
                onHide={this.props.modalClose}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header>
                    <Modal.Title id="contained-modal-title-vcenter">
                        User Details
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="show-grid">
                    
                        { 
                            this.props.user.map((u,index)=>{
                                return(
                                    
                                    <Container key={index}>
                                        <Row>
                                            <Col md={3}>
                                                <label className="col-md-12 pl-0" htmlFor="firstName">FirstName:</label>
                                                <input 
                                                className="col-md-12"
                                                type="text" 
                                                name="firstName" 
                                                id="firstName"
                                                value={this.props.user[index].fisrtName}
                                                onChange={e=>this.props.handleInputChange(e,index)}
                                                />
                                            </Col>
                                            <Col md={2}>
                                                <label className="col-md-12 pl-0" htmlFor="lastName">LastName:</label>
                                                <input 
                                                className="col-md-12" 
                                                type="text" 
                                                name="lastName" 
                                                id="lastName"
                                                value={this.props.user[index].lastName}
                                                onChange={e=>this.props.handleInputChange(e,index)}
                                                />
                                            </Col>
                                            <Col xs={12} md={3}>
                                                <label className="col-md-12 pl-0" htmlFor="email">Email:</label>
                                                <input 
                                                className="col-md-12" 
                                                type="email" 
                                                name="email" 
                                                id="email"
                                                value={this.props.user[index].email}
                                                onChange={e=>this.props.handleInputChange(e,index)}
                                                />
                                            </Col>
                                            <Col xs={12} md={2}>
                                                <label className="col-md-12 pl-0" htmlFor={"vaccinated"+index}>Vaccinated:</label>
                                                 
                                                  <RadioGroup className="col-md-12" 
                                                  id={"vaccinated" + index} 
                                                  name={"vaccinated"+index}
                                                  >
                                                    <div className="radio-button-background">
                                                        <Radio value="Yes" 
                                                        className="radio-button"
                                                        onChange={e=>this.props.handleInputChange(e,index)}

                                                        checked={this.props.user[index]["vaccinated"+index]==='Yes'}
                                                        />Yes
                                                    </div>
                                                    <div className="radio-button-background">
                                                        <Radio 
                                                        value="No"
                                                        onChange={e=>this.props.handleInputChange(e,index)}
                                                        className="radio-button" 
                                                        checked={this.props.user[index]["vaccinated"+index]==='No'}
                                                        />No
                                                    </div>
                                                </RadioGroup>  
                                            </Col>
                                            <Col md={2} className="mt-4">
                                                <Button className="px-2 rounded-pill py-0" variant="danger" size="sm" onClick={(e)=>this.props.deleteUser(e,index)}>x</Button>
                                            </Col>
                                        </Row>
                                        
                                </Container>
                                );
                            })
                        }
                    <Container>
                        <Row>
                            
                            <Col md={2}>
                                <Button 
                                className="px-2 py-0" 
                                disabled={empt}
                                variant="primary" 
                                size="sm" 
                                onClick={(e)=>this.props.addUser(this.props.user.length)}>+</Button>
                            </Col>
                        </Row>
                    </Container>
                </Modal.Body>
                <Modal.Footer className="justify-content-center">
                    <Button className="px-5" onClick={()=>this.props.confrimCheck()}>Ok</Button>
                    <Button className="px-5 btn-danger" onClick={this.props.modalClose}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}
 
export default ModalsAdd; 